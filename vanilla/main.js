const calculator = document.createElement("DIV");
const screen = document.createElement("INPUT");
screen.setAttribute("readonly", true);
screen.setAttribute("class", "screen");
screen.setAttribute("id", "screen");
calculator.appendChild(screen);

drawKeypad();
addlastRowToKeypad();

window.screenValue = 0;
const updateScreen = () => {
    const screenToUpdate = document.getElementById("screen");
    screenToUpdate.value = window.screenValue;
}





const mount = document.getElementById("mount");
mount.appendChild(calculator);
updateScreen();





function drawKeypad() {
    const operators = createOperators();
    for (let i = 2; i >= 0; i--) {
        const numberRow = document.createElement("DIV");
        numberRow.setAttribute("class", "numberRow");
        for (let j = 1; j <= 3; j++) {
            const numberButton = document.createElement("SPAN");
            const number = 3 * i + j;
            numberButton.appendChild(document.createTextNode(number));
            numberButton.setAttribute("class", "numberButton");
            const buttonEvent = () => {
                window.screenValue = window.screenValue * 10 + number;
                updateScreen();
            };
            numberButton.addEventListener("click", buttonEvent);
            numberRow.appendChild(numberButton);
        }
        numberRow.appendChild(operators.pop())
        calculator.appendChild(numberRow);
    }
}

function addlastRowToKeypad() {
    const lastRow = document.createElement("DIV");
    lastRow.setAttribute("class", "numberRow");
    const numberButton = document.createElement("SPAN");
    numberButton.appendChild(document.createTextNode(0));
    numberButton.setAttribute("class", "numberButton");
    const buttonEvent = () => {
        window.screenValue = window.screenValue * 10;
        updateScreen();
    };
    numberButton.addEventListener("click", buttonEvent);
    lastRow.appendChild(numberButton);

    const clearButton = document.createElement("SPAN");
    clearButton.appendChild(document.createTextNode('C'));
    clearButton.setAttribute("class", "numberButton");
    const clearEvent = () => {
        window.calculatorValue = 0;
        window.screenValue = 0;
        updateScreen();
    };
    clearButton.addEventListener("click", clearEvent);
    lastRow.appendChild(clearButton);

    const equalButton = document.createElement("SPAN");
    equalButton.appendChild(document.createTextNode('='));
    equalButton.setAttribute("class", "numberButton");
    const equalEvent = () => {
        calculate();
    };
    equalButton.addEventListener("click", equalEvent);
    lastRow.appendChild(equalButton);

    const addButton = document.createElement("SPAN");
    addButton.appendChild(document.createTextNode('+'));
    addButton.setAttribute("class", "numberButton");
    const addEvent = () => {
        calculate();
        window.operator = "add";        
    };
    addButton.addEventListener("click", addEvent);
    lastRow.appendChild(addButton);

    calculator.appendChild(lastRow);

}

function createOperators() {
    const divideButton = document.createElement("SPAN");
    divideButton.appendChild(document.createTextNode('/'));
    divideButton.setAttribute("class", "numberButton");
    const divideEvent = () => {
        calculate();
        window.operator = "divide";
    };
    divideButton.addEventListener("click", divideEvent);

    const multiplyButton = document.createElement("SPAN");
    multiplyButton.appendChild(document.createTextNode('X'));
    multiplyButton.setAttribute("class", "numberButton");
    const multiplyEvent = () => {
        calculate();
        window.operator = "multiply";
    };
    multiplyButton.addEventListener("click", multiplyEvent);

    const subtractButton = document.createElement("SPAN");
    subtractButton.appendChild(document.createTextNode('-'));
    subtractButton.setAttribute("class", "numberButton");
    const subtractEvent = () => {
        calculate();
        window.operator = "subtract";
    };
    subtractButton.addEventListener("click", subtractEvent);

    return [subtractButton, multiplyButton, divideButton];
}

function calculate() {
    if (window.operator === "add") {
        window.calculatorValue += window.screenValue;
    }
    else if (window.operator === "subtract") {
        window.calculatorValue -= window.screenValue;
    }
    else if (window.operator === "multiply") {
        window.calculatorValue *= window.screenValue;
    }
    else if (window.operator === "divide") {
        window.calculatorValue /= window.screenValue;
    }
    else {
        window.calculatorValue = window.screenValue;
    }
    document.getElementById("screen").value = window.calculatorValue;
    window.screenValue = 0;
}
